# react-express-app

`react-express-app` is a default template for combining react frontend development work with an express backend. For smaller projects, it provides the flexibility to host the frontend and backend on the same server, while maintaining a simple and easy to follow development flow. For larger projects, frontend and backend tasks are easily separated through the `backend` command and `dist/` folder. In order to allow for hot reloading of the frontend on the backend server, this does not rely on the `proxy-pass` directive from the `create-react-app` utility. In fact, it does not use `create-react-app` at all. This setup opens opportunities to use websockets and other non supported protocols while still maintaining a hospitable development environment. `Jest` configuration is also provided out of the box for unit and integration testing.

## Quickstart

Clone the project, then install the dependencies:

```
# using npm
npm install

# using yarn
yarn install
```

Then, startup the development server using:

```
npm run dev
```

This serves the app on `http://0.0.0.0:8000`

To override the default bind address, use the `HOST` and `PORT` environment variables:

```
HOST=127.0.0.1 PORT=8888 npm run dev
```

## Usage

The two main folders to look at are `src/` and `react/`. The backend and all associated files are meant to be in the `src/` tree, while the frontend and all associated files are meant to be in the `react/` tree. Project configuration files are housed in the project root.

### Commands

A set of simple commands are provided out of the box
```
npm run dev             Start the server in development mode
                        - enables hot reloading
                        - enables error messages displayed to end user
                        - disables minification/compression
npm run build           Build the frontend and place output to dist/ folder
npm run start           Run the project from the dist/ folder (must run build first)
npm run start:frontend  Run only the dist/ folder (aka disable main/backend)
npm run start:backend   Run only the main function (aka disable react)
npm run test            Run all unit/integration tests with jest
npm run test:watch      Run jest in watch mode
```

### React

For the most part, the setup is similar to the layout that `create-react-app` provides if you substitute the `src/` folder to `react/`. All public html, js, css, or other files housed in the `public/` folder are served as is without modification.

The `react/index.js` file renders the `App` component (`react/App.js`) in the `#root` div from `public/index.html`. There is little to no deviance from `create-react-app` here so any tutorials or documentation for those should just work. For the most part, the `public/index.html` file does not need to be modified, except maybe to add script or style imports.

This project is intended to be used with `react-router`. For this reason, any path that is not resolved (e.g. any path that would return a 404 not found error) is served to the react app for handling. If this behavior is not desired, set the `NO_REWRITE` environment variable to `true` before running:

```
NO_REWRITE=true npm run start
```

### Express

Some of the express app, hot reloading, and error handling is controlled in the main `serve.js` file. The majority of use cases should not need to edit these. Instead, the `app` and `server` objects are passed to a `main` function located in `src/main.js`. That is the place for you to start adding any API configurations. The `main` function is called before any react paths are added to the project, so any routes or middlewares placed in main will take effect over the entire project.

Note: ES6 syntax is not supported with express out of the box (only react)

### Testing

This template sets up tests for both react and express. See the sample tests in `react/App.test.js` and `src/main.test.js` for examples. Also refer to the jest documentation for full features.

This template also comes pre-packaged with the `react-testing-library`. If another testing library is desired, simply delete the `@testing-library/*` packages from the `package.json` file before installing (and setup new tests).

### Defaults

The `defaults.js` file contains the default variables for the project. Change these as desired, but anything other than `HOST` or `PORT` may cause unexpected behavior.

## Known Issues

* When running test with jest, importing css modules without extensions is not directly supported. Instead, you have to add an exception in the jest configuration to mock out those files. See `moduleNameMapper` in `package.json` for an example. An exception has already been added for the `typeface-` family of font packages. Alternatively, these files can be imported directly, such as `import "bootstrap/dist/css/boostrap.css` (aka with an extension).
* Let me know :)
