const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);

// noop-out browserify in production environment
const browserify = (process.env.BUILD || (process.env.NODE_ENV !== "production"))?
  require("browserify-middleware") : () => ({});

if (!process.env.FRONTEND_ONLY && !process.env.BUILD)
  // pass app and server instances to main function
  require("./src/main")(app, server);

const {
  HOST,
  PORT,
  PUBLIC_DIR,
  DIST_DIR,
  REACT_ROOT,
  BUNDLE_PATH,
  BUNDLE_EXPORT_PATH,
  INDEX_FILE,
  APP_ROOT,
  BROWSERIFY_OPTIONS,
} = require("./defaults");
const { emitter, ...browserifyOptions } = BROWSERIFY_OPTIONS(app);

// custom-errorhandler middleware for dev mode
function errorhandler(err, req, res, next) {
  if (process.env.BUILD) {
    console.log('\n', err.stack, '\n');
    return;
  }
  const jsescape = require("js-string-escape");
  const AU = require("ansi_up")
  const ansi_up = new AU.default;
  return res.send(`
    document.body.style.padding = "10px 20px";
    document.body.style.background = "#111";
    document.body.style.color = "#bff";
    document.body.style.overflow = "auto";
    document.body.innerHTML =
      "<pre>${jsescape(ansi_up.ansi_to_html(err.stack))}</pre>";
  `);
}

const handleBundle = browserify(REACT_ROOT, browserifyOptions);

// rewrite '/' as "index.html" and automatically add .html to files
const staticConf = {
  index: [INDEX_FILE],
  extensions: ["html"]
};

// support disabling frontend altogether
if (!process.env.BACKEND_ONLY) {

  // default staticPath to distribution directory
  let staticPath = DIST_DIR;

  // when not in production...
  if (process.env.NODE_ENV !== "production") {

    // ... serve files dynamically and hot reload
    app.use("/" + BUNDLE_PATH, handleBundle);

    // ... use the unmodified files from public
    staticPath = PUBLIC_DIR;

    // ... output (semi) nicely formatted error messages
    app.use(errorhandler);

  }

  // serve static files
  app.use(express.static(staticPath, staticConf));

  if (!process.env.NO_REWRITE) {

    // 404: rewrite to / and hope static catches the index
    function catchAll() {
      app.use(function(req, res, next) {
        req.url = INDEX_FILE;
        next();
      });
      app.use(express.static(staticPath, staticConf));
    }

    // wait for bundle to be generated before adding catch-all
    emitter.once("compiled", catchAll);

    // always add catch all in production
    if (process.env.NODE_ENV === "production")
      catchAll();

  }

}

if (process.env.BUILD) {
  console.log("generating production bundle...");

  // catch and print errors in the build process
  process.on("beforeExit", () => {
    const middlewareApp = express();
    middlewareApp.use("/" + BUNDLE_PATH, handleBundle);
    middlewareApp.use(errorhandler);

    // simulate a request to browserify-middleware
    middlewareApp._router.handle(
      {url: "/" + BUNDLE_PATH},
      new Proxy(new Object(), {get: () => (() => {})}),
      () => {}
    );
  });

  emitter.on("done", () => {
    process.exit()
  });
}
else {
  server.listen(PORT, HOST, () => {
    console.log(`server started on ${HOST}:${PORT}`);
  });
}
