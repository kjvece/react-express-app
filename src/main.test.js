const app = require("express")();
const http = require("http");
const server = http.createServer(app);

const HOST = "127.0.0.1";
const PORT = 9999;
const URL = `http://${HOST}:${PORT}`;

beforeAll(() => {
  require("./main")(app, server);
  server.listen(PORT, HOST);
});

afterAll(() => {
  server.close();
});

test("/api/health", (done) => {
  http.get(`${URL}/api/health`, (res) => {
    res.setEncoding("utf8");
    res.on("data", data => {
      expect(data).toBe("OK");
      done();
    })
  });
});
