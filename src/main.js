module.exports = function main(app, server) {

  app.get("/api/health", (req, res) => {
    return res.send("OK");
  });

}
