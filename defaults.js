const path = require("path");

const staticExports = {
  HOST: process.env.HOST || "0.0.0.0",
  PORT: process.env.PORT || 8000,
  PUBLIC_DIR: process.env.PUBLIC_DIR || path.join(__dirname, "public"),
  DIST_DIR: process.env.DIST_DIR || path.join(__dirname, "dist"),
  REACT_ROOT: "./react/index.js",
  BUNDLE_PATH: "bundle.js",
  INDEX_FILE: "index.html",
  APP_ROOT: "root",
}

let fs;
if (process.env.BUILD)
  fs = require("fs-extra");

let BUNDLE_EXPORT_PATH = path.join(staticExports.DIST_DIR, "bundle.js")

module.exports = {
  ...staticExports,
  BUNDLE_EXPORT_PATH,
  BROWSERIFY_OPTIONS: (app) => {
    const events = require("events");
    const emitter = new events.EventEmitter();
    return {
      emitter,
      transform: [
        [
          "babelify", {
            presets: [
              "@babel/preset-env",
              "@babel/preset-react",
             ],
           }
        ],
        [
          "browserify-css", {
            global: true,
            minify: process.env.NODE_ENV === "production",
            stripComments: process.env.NODE_ENV === "production",
            inlineImages: true,

            // function to copy unhandle-able files by directly including them
            processRelativeUrl: function(relativeUrl) {
              const stripQueryStringAndHashFromPath = function(url) {
                return url.split('?')[0].split('#')[0];
              };
              const relativePath = stripQueryStringAndHashFromPath(relativeUrl);
              const queryStringAndHash = relativeUrl.substring(relativePath.length);

              // only node_modules should be un-includable (such as fonts)
              const prefix = "node_modules/";
              if (relativePath.startsWith(prefix)) {
                const route = relativePath.substring(prefix.length);
                const distPath = path.join(
                  staticExports.DIST_DIR,
                  route,
                );
                const fullQuery = route + queryStringAndHash;

                const source = path.join(process.cwd(), relativePath);
                const target = path.resolve(distPath);

                // copy files or add a route handler
                if (process.env.BUILD)
                  fs.copySync(source, target);
                else {
                  app.get("/" + route, (req, res) => {
                    return res.sendFile(source);
                  });
                }

                // return the new route
                return "/" + fullQuery;
              }

              return relativeUrl;
            }
          }
        ],
      ],
      postcompile: function(src) {
        emitter.emit("compiled");
        return src;
      },
      postminify: function(src) {
        // when building, copy the bundle
        if (process.env.BUILD) {
          console.log(`generated ${BUNDLE_EXPORT_PATH}:`, src.length, "bytes")
          fs.copySync(staticExports.PUBLIC_DIR, staticExports.DIST_DIR);
          fs.writeFileSync(BUNDLE_EXPORT_PATH, src);
          console.log('\nFinished! Use "npm run start" to serve the files')
        }

        // signal that build is complete
        emitter.emit("done");
        return src;
      },
    };
  },
}
